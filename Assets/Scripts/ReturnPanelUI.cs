﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using System.Collections;

public class ReturnPanelUI : MonoBehaviour 
{
	#region Inspector
	
	
	
	#endregion //Inspector
	

	
	#region Unity Engine & Events
	
	
	
	#endregion //Unity Engine & Events

    /// <summary>
    /// Exit from current room and load a level
    /// </summary>
    /// <param name="levelToLoad">The name of the level to load</param>
    public void ExitRoom(string levelToLoad)
    {
        PhotonNetwork.LeaveRoom();
        ScreenFader.instance.LoadLevel(levelToLoad);
    }
	
    public void LoadLevel(string levelToLoad)
    {
        ScreenFader.instance.LoadLevel(levelToLoad);
    }

}
