﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;

/// <summary>
/// Show the current avatar in an image component
/// </summary>
public class DisplayAvatar : MonoBehaviour 
{
    #region Inspector



    #endregion //Inspector

    public int SavedAvatarIndex
    {
        get
        {
            return PlayerPrefs.GetInt("AvatarIndex", 0);
        }
        set
        {
            PlayerPrefs.SetInt("AvatarIndex", value);
        }
    }

    private Image _image;
    private AvatarList _avatarList;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _image = GetComponent<Image>();
        _avatarList = Resources.Load<AvatarList>("Avatar List");
    }

    private void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        //The array 0 is the player that has been updated, the 1 is the updated hastable
        //The first is commented out because we don't use the player
        //PhotonPlayer updatedPlayer = (PhotonPlayer)playerAndUpdatedProps[0];
        Hashtable updatedProperties = (Hashtable)playerAndUpdatedProps[1];

        object value;
        updatedProperties.TryGetValue("avtr", out value);
        SavedAvatarIndex = int.Parse(value.ToString());
        SetAvatarSprite(_avatarList.GetAvatarIndex(SavedAvatarIndex));
    }

    private void Start()
    {
        LoadUserAvatar();

        int avatarIndex = GetAvatarIndex(PhotonNetwork.player.customProperties);
        SetAvatarSprite(_avatarList.GetAvatarIndex(avatarIndex));
    }

    #endregion //Unity Engine & Events

    private void LoadUserAvatar()
    {
        //set an initial avatar as a player custom property
        Hashtable properties = new Hashtable();
        properties.Add("avtr", SavedAvatarIndex);
        PhotonNetwork.player.SetCustomProperties(properties);
    }

    private int GetAvatarIndex(Hashtable properties)
    {
        object value;
        bool success = properties.TryGetValue("avtr", out value);
        if (success)
        {            
            return int.Parse(value.ToString());
        }
        else
        {
            return 1;
        }
    }

    private void SetAvatarSprite(Sprite avatar)
    {
        _image.sprite = avatar;
    }
}
