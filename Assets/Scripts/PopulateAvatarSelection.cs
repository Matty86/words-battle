﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class PopulateAvatarSelection : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject avatarSelectTemplate;

    #endregion //Inspector

    private ScrollRect _scrollView;
    private AvatarList _avatarList;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _scrollView = GetComponent<ScrollRect>();
        _avatarList = Resources.Load<AvatarList>("Avatar List");
    }

    private void Start()
    {
        PopulateScrollView();
    }
	
	#endregion //Unity Engine & Events
	
    private void PopulateScrollView()
    {
        for(int i = 0; i < _avatarList.avatars.Length; i++)
        {
            GameObject spawnedAvatar = (GameObject)Instantiate(avatarSelectTemplate, _scrollView.content, false);
            SelectAvatar _selectAvatar = spawnedAvatar.GetComponentInChildren<SelectAvatar>();
            _selectAvatar.AvatarIndex = i;
        }
    }

}
