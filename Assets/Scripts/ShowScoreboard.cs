﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class ShowScoreboard : MonoBehaviour
{
    public delegate void Events();
    public static event Events OnScoreboardShown;
    public static event Events OnScoreboardHide;

    #region Inspector

    [SerializeField]
    private GameObject scorebordUI;

    [Tooltip("The amount of seconds the scoreboard will be shown")]
    public float showDuration = 5f;

    #endregion //Inspector

    private PhotonView _photonView;
    private RoomTurns _roomTurns;

    #region Unity Engine & Events

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _roomTurns = GetComponentInParent<RoomTurns>();
    }

    private void OnEnable()
    {
        Countdown.OnCountdownOver += Countdown_OnCountdownOver;
        StartNextTurnButton.OnNextButtonClick += StartNextTurnButton_OnNextButtonClick;
    }

    private void StartNextTurnButton_OnNextButtonClick()
    {
        _photonView.RPC("StartNextGame", PhotonTargets.All);           
    }

    private void Countdown_OnCountdownOver()
    {
        EnableScoreboard();
    }

    private void OnDisable()
    {
        Countdown.OnCountdownOver -= Countdown_OnCountdownOver;
        StartNextTurnButton.OnNextButtonClick -= StartNextTurnButton_OnNextButtonClick;
    }

    #endregion //Unity Engine & Events

    private void EnableScoreboard()
    {
        scorebordUI.SetActive(true);

        if (OnScoreboardShown != null) OnScoreboardShown();       
    }

    [PunRPC]
    private void StartNextGame()
    {
        if (_roomTurns.Turns > 0)
        {
            scorebordUI.SetActive(false);
            if (OnScoreboardHide != null) OnScoreboardHide();
        }
        else
        {
            ScreenFader.instance.LoadLevel("Game");
        }
    }

}
