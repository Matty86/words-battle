﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class WinnerPlayer : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private Text playerNameText;

    [SerializeField]
    private Image playerAvatarImage;

    #endregion //Inspector

    private AvatarList avatarList;
	
	#region Unity Engine & Events
	
    private void Awake()
    {
        avatarList = Resources.Load<AvatarList>("Avatar List");
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Set the winning player
    /// </summary>
    /// <param name="winningPlayer"></param>
    public void SetWinningPlayer(PhotonPlayer winningPlayer)
    {
        playerAvatarImage.sprite = avatarList.GetPlayerAvatar(winningPlayer);
        playerNameText.text = winningPlayer.name;
    }

}
