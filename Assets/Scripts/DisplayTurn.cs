﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Display the current turn
/// </summary>
public class DisplayTurn : MonoBehaviour 
{
    #region Inspector



    #endregion //Inspector

    private int currentTurn = 1;
    private int maxRoomTurns;

    private Text _turnText;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _turnText = GetComponent<Text>();
        maxRoomTurns = GetRoomTurnsAmount();
        UpdateTurnsText();
    }

    private void OnEnable()
    {
        ShowScoreboard.OnScoreboardShown += ShowScoreboard_OnScoreboardShown;
    }

    private void ShowScoreboard_OnScoreboardShown()
    {
        UpdateTurnsText();

        currentTurn++;
        Mathf.Clamp(currentTurn, 1, maxRoomTurns);
    }

    private void OnDisable()
    {
        ShowScoreboard.OnScoreboardShown -= ShowScoreboard_OnScoreboardShown;
    }

    #endregion //Unity Engine & Events

    private void UpdateTurnsText()
    {
        _turnText.text = " TURN " + currentTurn + " / " + maxRoomTurns;
    }

    private int GetRoomTurnsAmount()
    {
        object value;
        PhotonNetwork.room.customProperties.TryGetValue("turns", out value);
        return int.Parse(value.ToString());
    }

}
