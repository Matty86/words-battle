﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class RoomElementParameters : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private Text roomName;

    [SerializeField]
    private Text roomLanguage;
	
	#endregion //Inspector
	   
    public RoomInfo RoomInfo
    {
        get; set;
    }
	
	#region Unity Engine & Events
	
	
	
	#endregion //Unity Engine & Events

    /// <summary>
    /// Set the room name
    /// </summary>
    /// <param name="name">The name of the room</param>
    public void SetRoomName(string name)
    {
        roomName.text = name;
    }
	
    /// <summary>
    /// Set the room language
    /// </summary>
    /// <param name="language">The language</param>
    public void SetRoomLanguage(string language)
    {
        roomLanguage.text = "Language: \n" + language;
    }

    /// <summary>
    /// Get the name of the room
    /// </summary>
    /// <returns>The name of room</returns>
    public string GetRoomName()
    {
        return roomName.text;
    }

    /// <summary>
    /// Get the room language
    /// </summary>
    /// <returns>The language of the room</returns>
    public string GetRoomLanguage()
    {
        return roomLanguage.text;
    }

    /// <summary>
    /// Return the password of the room as a string
    /// </summary>
    /// <returns>A string password</returns>
    public string GetPassword()
    {
        if (RoomInfo.customProperties.ContainsKey("pwd"))
        {
            object value;
            RoomInfo.customProperties.TryGetValue("pwd", out value);
            return value.ToString();
        }
        else
        {
            return string.Empty;
        }
    }

}
