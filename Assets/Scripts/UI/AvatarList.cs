﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

[CreateAssetMenu(fileName = "Avatar List")]
public class AvatarList : ScriptableObject
{
    #region Inspector

    public Sprite[] avatars;

    #endregion //Inspector

    #region Unity Engine & Events    

    #endregion //Unity Engine & Events

    /// <summary>
    /// Return the avatar in the list at index
    /// </summary>
    /// <param name="index">The avatar index</param>
    /// <returns></returns>
    public Sprite GetAvatarIndex(int index)
    {
        return avatars[index];
    }

    /// <summary>
    /// Get the avatr of the player
    /// </summary>
    /// <param name="player">The player we want to get the avatar</param>
    /// <returns></returns>
    public Sprite GetPlayerAvatar(PhotonPlayer player)
    {
        object value;
        player.customProperties.TryGetValue("avtr", out value);
        int avatarIndex = int.Parse(value.ToString());
        return GetAvatarIndex(avatarIndex);
    }

}
