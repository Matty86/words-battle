﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

/// <summary>
/// Simple class that contains basic events to be called from
/// inspector on UI events
/// </summary>
public class UIEvents : MonoBehaviour 
{
    public void Quit()
    {
        Application.Quit();
    }
}
