﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ExitGames.Client.Photon;

public class SelectAvatar : MonoBehaviour, IPointerClickHandler
{
    public delegate void SelectAvatarEvent(Sprite selectedAvatar);
    public static event SelectAvatarEvent OnAvatarSelected;

    #region Inspector
    

    #endregion //Inspector

    public int AvatarIndex { get; set; }

    private Image _image;
    private AvatarList avatarList;

    #region Unity Engine & Events

    private void Awake()
    {
        _image = GetComponent<Image>();
        avatarList = Resources.Load<AvatarList>("Avatar List");
    }

    private void Start()
    {
        //Set the avatar of this template
        _image.sprite = avatarList.GetAvatarIndex(AvatarIndex);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SelectCurrentAvatar();
    }

    #endregion //Unity Engine & Events

    private void SelectCurrentAvatar()
    {
        Hashtable properties = new Hashtable();
        properties.Add("avtr", AvatarIndex);
        PhotonNetwork.player.SetCustomProperties(properties);
        if (OnAvatarSelected != null) OnAvatarSelected(avatarList.GetAvatarIndex(AvatarIndex));
    }

}
