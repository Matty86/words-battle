﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;

public class RoomCreation : MonoBehaviour 
{
    #region Inspector

    [Header("Settings")]

    [SerializeField]
    private byte maxPlayers;

    [Header("References")]

    [SerializeField]
    private InputField nameField;

    [SerializeField]
    private Toggle passwordToggle;

    [SerializeField]
    private InputField passwordField;

    [SerializeField]
    private Dropdown languageDropdown;

    [SerializeField]
    private InputField turnDurationField;

    [SerializeField]
    private InputField turnAmountField;

    [SerializeField]
    private InputField categoriesAmountField;

    [SerializeField]
    private Button createRoomButton;

    #endregion //Inspector

    private CategoriesFiller _categoriesFiller;
	
	#region Unity Engine & Events

    private void Awake()
    {
        _categoriesFiller = GetComponentInChildren<CategoriesFiller>();
    }

    private void OnEnable()
    {
        AddCategory.OnCategoryAdded += AddCategory_OnCategoryAdded;
    }

    private void AddCategory_OnCategoryAdded()
    {
        FilledField();
    }

    private void OnDisable()
    {
        AddCategory.OnCategoryAdded -= AddCategory_OnCategoryAdded;
    }

    private void Start()
    {
        createRoomButton.interactable = IsEverythingFilled();
    }

    private void OnCreatedRoom()
    {
        ScreenFader.instance.LoadLevel("Game");
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Create a room on Photon Network
    /// </summary>
    public void CreateRoom()
    {
        if (!PhotonNetwork.connected)
        {
            Debug.LogWarning("<color=orange>You are not connected to photon!</color>");
            return;
        }

        RoomOptions options = new RoomOptions();
        options.MaxPlayers = maxPlayers;

        //set room parameters
        Hashtable customRoomProperties = new Hashtable();

        customRoomProperties.Add("time", turnDurationField.text);
        customRoomProperties.Add("turns", turnAmountField.text);
        customRoomProperties.Add("catN", categoriesAmountField.text);

        //add all categories in format: key = cat1, value = categoryname
        for (int i = 0; i < int.Parse(categoriesAmountField.text); i++)
        {
            customRoomProperties.Add("cat" + i, _categoriesFiller.categories[i]);
        }

        //Add password if there is one
        if(passwordToggle.isOn)
        {
            options.CustomRoomPropertiesForLobby = new string[] { "pwd" };
            customRoomProperties.Add("pwd", passwordField.text);
        }

        options.CustomRoomProperties = customRoomProperties;

        TypedLobby lobby = new TypedLobby(languageDropdown.value.ToString(), LobbyType.Default);
        bool succes = PhotonNetwork.CreateRoom(nameField.text, options, lobby);
        if (succes)
        {
            // We just created the room so we are the host
            Debug.Log("<color=green>Room Successfully created!</color>");
            if (!PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.SetMasterClient(PhotonNetwork.player);
            }
            GameData.isHost = true;
        }     
        else
        {
            Debug.LogWarning("<color=orange>Couldn't create room, try again!</color>");
        } 
    }

	public void FilledField()
    {
        createRoomButton.interactable = IsEverythingFilled();
    }

    private bool IsEverythingFilled()
    {
        if(passwordToggle.isOn && passwordField.text == string.Empty)
        {
            return false;
        }

        if (nameField.text == string.Empty)
        {
            return false;
        }
        else if (turnDurationField.text == string.Empty)
        {
            return false;
        }
        else if (turnAmountField.text == string.Empty)
        {
            return false;
        }
        else if (categoriesAmountField.text == string.Empty)
        {
            return false;
        }
        else if (_categoriesFiller.categories.Count < int.Parse(categoriesAmountField.text))
        {
            return false;
        }
        return true;
    }
}
