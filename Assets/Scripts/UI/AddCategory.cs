﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class AddCategory : MonoBehaviour
{
    public delegate void AddCategoryEvent();
    public static event AddCategoryEvent OnCategoryAdded;

    #region Inspector



    #endregion //Inspector

    private InputField _inputField;
    private CategoriesFiller _categoriesFiller;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _inputField = GetComponent<InputField>();
        _categoriesFiller = GetComponentInParent<CategoriesFiller>();        
    }

    private void OnEnable()
    {
        _inputField.onEndEdit.AddListener(delegate { AddCategoryToList(); });
    }

    private void OnDisable()
    {
        _inputField.onEndEdit.RemoveAllListeners();
    }
	
	#endregion //Unity Engine & Events
	
    private void AddCategoryToList()
    {
        _categoriesFiller.categories.Add(_inputField.text);
        if (OnCategoryAdded != null) OnCategoryAdded();
    }

}
