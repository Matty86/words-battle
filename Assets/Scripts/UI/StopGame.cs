﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(PhotonView))]
public class StopGame : MonoBehaviour 
{
    public delegate void Event();
    public static event Event OnAllPlayersStopped;

    #region Inspector



    #endregion //Inspector

    private int stoppedPlayers;

    private PhotonView _photonView;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
    }

    private void OnEnable()
    {
        StartNextTurnButton.OnNextButtonClick += StartNextTurnButton_OnNextButtonClick;
    }

    private void StartNextTurnButton_OnNextButtonClick()
    {
        stoppedPlayers = 0;
    }

    private void OnDisable()
    {
        StartNextTurnButton.OnNextButtonClick -= StartNextTurnButton_OnNextButtonClick;
    }

    #endregion //Unity Engine & Events

    public void Stop()
    {
        _photonView.RPC("PlayerStopped", PhotonTargets.MasterClient);
    }

    [PunRPC]
    private void PlayerStopped()
    {
        stoppedPlayers ++;
        if (stoppedPlayers < PhotonNetwork.playerList.Length)
        {
            return;
        }
        else
        {
            if (OnAllPlayersStopped != null) OnAllPlayersStopped();
        }
    }

}
