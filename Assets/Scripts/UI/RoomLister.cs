﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Fill a scrollview with available rooms to join
/// </summary>
public class RoomLister : MonoBehaviour
{
    #region Inspector

    [SerializeField]
    private GameObject roomTemplate;

    [SerializeField]
    private Dropdown languageFilterDropdown;

    #endregion //Inspector

    private ScrollRect _scrollRect;

    #region Unity Engine & Events

    private void Awake()
    {
        _scrollRect = GetComponent<ScrollRect>();
    }

    private void OnConnectedToMaster()
    {
        JoinLobby();
    }

    private void OnReceivedRoomListUpdate()
    {
        UpdateRooms();
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Join the lobby based on the current language filter
    /// </summary>
    public void JoinLobby()
    {
        TypedLobby lobby = new TypedLobby(languageFilterDropdown.value.ToString(), LobbyType.Default);
        bool success = PhotonNetwork.JoinLobby(lobby);
        if (success)
        {
            Debug.Log("<color=green>Successfully joined the lobby!</color>");
        }
        else
        {
            Debug.Log("<color=orange>Couldn't join a lobby!</color>");
        }
    }

    /// <summary>
    /// Update the scrollview with available rooms
    /// </summary>
    public void UpdateRooms()
    {
        ClearRooms();

        if (PhotonNetwork.insideLobby)
        {
            RoomInfo[] rooms = PhotonNetwork.GetRoomList();

            for (int i = 0; i < rooms.Length; i++)
            {
                if (rooms[i].open)
                {
                    //spawn a room template as child of content
                    GameObject room = (GameObject)Instantiate(roomTemplate, _scrollRect.content.transform, false);
                    var roomParameter = room.GetComponent<RoomElementParameters>();
                    //set room labels parameters
                    roomParameter.SetRoomName(rooms[i].name);
                    roomParameter.RoomInfo = rooms[i];
                    SystemLanguage roomLanguage = (SystemLanguage)languageFilterDropdown.value;
                    roomParameter.SetRoomLanguage(roomLanguage.ToString());

                    CheckForBannedRoom(rooms[i], room);
                }
            }
        }
        else
        {
            JoinLobby();
            Debug.Log("<color=orange>Can't update rooms, you are not inside a lobby!</color>");
        }
    }

    private void CheckForBannedRoom(RoomInfo roomInfo, GameObject spawnedRoomTemplate)
    {
        for(int i = 0; i < BannedRoomsList.bannedRooms.Count; i++)
        {
            if(BannedRoomsList.bannedRooms[i] == roomInfo.name)
            {
                Destroy(spawnedRoomTemplate);
            }
        }
    }

    private void ClearRooms()
    {
        //destroy all childs of contents
        for(int i = 0; i < _scrollRect.content.childCount; i++)
        {
            Destroy(_scrollRect.content.transform.GetChild(i).gameObject);
        }
    }

    
}
