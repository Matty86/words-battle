﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

public class CloseAvatarSelection : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject avatarSelectionUI;
	
	#endregion //Inspector
	

	
	#region Unity Engine & Events
	
	private void OnEnable()
    {
        SelectAvatar.OnAvatarSelected += SelectAvatar_OnAvatarSelected;
    }

    private void SelectAvatar_OnAvatarSelected(Sprite selectedAvatar)
    {
        avatarSelectionUI.SetActive(false);
    }

    private void OnDisable()
    {
        SelectAvatar.OnAvatarSelected -= SelectAvatar_OnAvatarSelected;
    }

    #endregion //Unity Engine & Events

}
