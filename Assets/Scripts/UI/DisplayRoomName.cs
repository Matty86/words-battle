﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class DisplayRoomName : MonoBehaviour 
{
    #region Inspector



    #endregion //Inspector

    private Text _text;
	
	#region Unity Engine & Events
	
    private void Awake()
    {
        _text = GetComponent<Text>();
    }

	private void Start()
    {
        _text.text = PhotonNetwork.room.name;
    }
	
	#endregion //Unity Engine & Events
	
}
