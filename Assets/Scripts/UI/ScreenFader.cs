﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScreenFader : MonoBehaviour 
{
    public static ScreenFader instance
    {
        get;
        private set;
    }

    #region Variables
    [Header("Parameters")]
    public float fadeSpeed = 1.5f;

    private RawImage blackImage;
    private bool canFadingToBlack;
	#endregion
	
    void Awake()
    {
        Singleton();
        Cache();
    }

    void Start()
    {
        StartScene();
    }

    private void OnLeftRoom()
    {
        //when the user leaves a room reload the lobby
        LoadLevel("Lobby");
    }

    private void StartScene()
    {
        blackImage.enabled = true;
        blackImage.color = Color.black;
        StartCoroutine(FadeToClear());
    }    

    public void LoadLevel(string levelToLoad)
    {
        if (canFadingToBlack)
        {
            canFadingToBlack = false;
            blackImage.enabled = true;
            blackImage.color = Color.clear;
            StartCoroutine(FadeToBlack(levelToLoad));
        }
    }

    public void LoadLevel(int levelToLoad)
    {
        if (canFadingToBlack)
        {
            canFadingToBlack = false;
            blackImage.enabled = true;
            blackImage.color = Color.clear;
            StartCoroutine(FadeToBlack(levelToLoad));
        }
    }

    private IEnumerator FadeToBlack(string level)
    {
        float fadeAmount = 0f;
        
        while(fadeAmount < 1)
        {
            fadeAmount += fadeSpeed * Time.deltaTime;
            blackImage.color = Color.Lerp(Color.clear, Color.black, fadeAmount);
            yield return null;
        }

        PhotonNetwork.LoadLevel(level);
    }

    private IEnumerator FadeToBlack(int level)
    {
        float fadeAmount = 0f;

        while (fadeAmount < 1)
        {
            fadeAmount += fadeSpeed * Time.deltaTime;
            blackImage.color = Color.Lerp(Color.clear, Color.black, fadeAmount);
            yield return null;
        }

        PhotonNetwork.LoadLevel(level);
    }

    public IEnumerator FadeToClear()
    {       
        float fadeAmount = 0f;

        while(fadeAmount < 1)
        {
            fadeAmount += fadeSpeed * Time.deltaTime;
            blackImage.color = Color.Lerp(Color.black, Color.clear, fadeAmount);
            yield return null;
        }
        

        blackImage.enabled = false;
        canFadingToBlack = true;
    }

    public void Cache()
    {
        blackImage = GetComponent<RawImage>();
    }

    protected void Singleton()
    {
        // First we check if there are any other instances conflicting
        if (instance != null && instance != this)
        {
            // If that is the case, we destroy other instances
            Destroy(this.gameObject);
        }
        else
        {
            // Here we save our singleton instance
            instance = this;
        }
    }
}
