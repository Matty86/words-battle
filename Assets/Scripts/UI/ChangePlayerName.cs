﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Change the current player name
/// </summary>
public class ChangePlayerName : MonoBehaviour 
{
    #region Inspector



    #endregion //Inspector

    private string PlayerName
    {
        get
        {
            return PlayerPrefs.GetString("PlayerName");
        }
        set
        {
            PlayerPrefs.SetString("PlayerName", value);
        }
    }

    private InputField _inputField;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _inputField = GetComponent<InputField>();
    }

    private void Start()
    {
        _inputField.text = PlayerName;
        SetPlayerName();
    }
	
	#endregion //Unity Engine & Events
	
    /// <summary>
    /// Set the current photon player name
    /// </summary>
    public void SetPlayerName()
    {
        PhotonNetwork.playerName = _inputField.text;
        PlayerName = _inputField.text;
    }

}
