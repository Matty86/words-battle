﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Join the room when clicking
/// </summary>
public class ClickToJoinRoom : MonoBehaviour, IPointerClickHandler
{
    public delegate void AskPasswordEvent(string roomName, string roomPassword);
    public static event AskPasswordEvent OnAttempToJoinRoomWithPassword;

    #region Inspector



    #endregion //Inspector

    private RoomElementParameters roomParameters;

    #region Unity Engine & Events

    private void Awake()
    {
        roomParameters = GetComponent<RoomElementParameters>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        JoinRoom();
    }

    private void OnJoinedRoom()
    {
        ScreenFader.instance.LoadLevel("Game");
    }

    #endregion //Unity Engine & Events

    private void JoinRoom()
    {
        if (roomParameters.GetPassword() == string.Empty)
        {
            PhotonNetwork.JoinRoom(roomParameters.GetRoomName());
        }
        else
        {
            OpenPasswordEnterDialog();
        }
    }

    private void OpenPasswordEnterDialog()
    {
        if (OnAttempToJoinRoomWithPassword != null)
            OnAttempToJoinRoomWithPassword(roomParameters.GetRoomName(), roomParameters.GetPassword());
    }

}
