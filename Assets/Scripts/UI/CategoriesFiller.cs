﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CategoriesFiller : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    [Tooltip("the template to use input categories")]
    private GameObject template;

    [SerializeField]
    private InputField categoriesAmount;

    [SerializeField]
    private Transform content;

    #endregion //Inspector

    [HideInInspector]
    public List<string> categories = new List<string>();
	
	#region Unity Engine & Events
	
	
	
	#endregion //Unity Engine & Events
	
    /// <summary>
    /// Update the categories in the UI
    /// </summary>
    public void UpdateCategoriesUI()
    {
        //clear all categories before updating
        ClearCategories();

        for (int i = 0; i < int.Parse(categoriesAmount.text); i++)
        {
            Instantiate(template, content, false);
        }
    }

    private void ClearCategories()
    {
        for(int i = 0; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);
        }

        categories.Clear();
    }

}
