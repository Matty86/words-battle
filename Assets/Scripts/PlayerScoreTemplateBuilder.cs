﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using ExitGames.Client.Photon;

[RequireComponent(typeof(Player))]
public class PlayerScoreTemplateBuilder : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private Text playerNameField;

    [SerializeField]
    private Image avatarField;

    [SerializeField]
    private Text scoreField;

    [SerializeField]
    [Tooltip("The text component used to display the current position in the scoreboard against other players")]
    private Text positionField;

    #endregion //Inspector

    private Player _player;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _player = GetComponent<Player>();
    }

    private void OnEnable()
    {
        ShowScoreboard.OnScoreboardShown += ShowScoreboard_OnScoreboardShown;       
    }

    private void ShowScoreboard_OnScoreboardShown()
    {
        SetPlayerScore(_player.photonPlayer.GetScore().ToString());
    }

    private void OnDisable()
    {
        ShowScoreboard.OnScoreboardShown -= ShowScoreboard_OnScoreboardShown;
    }

    private void Start()
    {
        SetPlayerName(_player.photonPlayer.name);
        SetAvatar(_player.GetAvatar());
        SetPlayerScore(_player.photonPlayer.GetScore().ToString());
    }

    private void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        //PhotonPlayer player = playerAndUpdatedProps[0] as PhotonPlayer;
        //Hashtable properties = playerAndUpdatedProps[1] as Hashtable;

        SetPlayerScore(_player.photonPlayer.GetScore().ToString());
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Set the avatar in the avatar slot
    /// </summary>
    /// <param name="avatar">The avatar to set</param>
    public void SetAvatar(Sprite avatar)
    {
        avatarField.sprite = avatar;
    }

    /// <summary>
    /// Set the player
    /// </summary>
    /// <param name="name">The name of the player to set</param>
    public void SetPlayerName(string name)
    {
        playerNameField.text = name;
    }

    /// <summary>
    /// Set the score of the player
    /// </summary>
    /// <param name="score">The score to set</param>
    public void SetPlayerScore(string score)
    {
        scoreField.text = score;
        SetRankingPosition();
    }

    private void SetRankingPosition()
    {
        List<int> playerScores = new List<int>();

        for(int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            //Add the player score to the list
            playerScores.Add(PhotonNetwork.playerList[i].GetScore());
        }

        //sort the list descending order
        playerScores.Sort();
        playerScores.Reverse();

        SetRank(playerScores);
    }

    private void SetRank(List<int> playerScores)
    {
        for (int i = 0; i < playerScores.Count; i++)
        {
            if (playerScores[i] == _player.photonPlayer.GetScore())
            {
                positionField.text = (i + 1).ToString();
            }
        }
    }

}
