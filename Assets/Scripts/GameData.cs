﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Stores game data
/// </summary>
public static class GameData
{
    public static bool isHost = false;
    public static PhotonPlayer photonPlayer;
    public static string playerName;
    public static int photonPlayerID;
    public static Sprite playerAvatar;
}
