﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class StartGameButton : MonoBehaviour
{
    public delegate void Event();
    public static event Event OnStartButtonClick;

    #region Inspector



    #endregion //Inspector

    private Button _button;

    #region Unity Engine & Events

    private void Awake()
    {
        _button = GetComponent<Button>();
    }

    private void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        SetInteractable();
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        SetInteractable();
    }

    private void Start()
    {
        SetInteractable();
    }

    #endregion //Unity Engine & Events

    public void StartGame()
    {
        ResetPlayerScores.ResetAllScores();
        if (OnStartButtonClick != null) OnStartButtonClick();

        //the game is started so close the room
        PhotonNetwork.room.open = false;

        _button.gameObject.SetActive(false);
    }

    private void SetInteractable()
    {
        //Set to button to interactable state if there is more the 1 player in the room
        _button.interactable = PhotonNetwork.playerList.Length > 1;
    }

}
