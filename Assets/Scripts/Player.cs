﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

public class Player : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject leaderMark;

    [SerializeField]
    private GameObject kickButton;

    #endregion //Inspector

    /// <summary>
    /// The current Photon Player data, it should be set when this object is instantiated
    /// </summary>
    public PhotonPlayer photonPlayer;

    private AvatarList _avatarList;
    private PhotonView _photonView;
	
	#region Unity Engine & Events
	
    private void Awake()
    {
        _avatarList = Resources.Load<AvatarList>("Avatar List");
        _photonView = GetComponent<PhotonView>();
    }

	private void Start()
    {
        // Remove the kick button at start if this is not the master client
        if(!PhotonNetwork.isMasterClient && kickButton)
        {
            kickButton.SetActive(false);
        }
        //enable the leader mark if this player is the master client
        if(photonPlayer.isMasterClient)
        {
            leaderMark.SetActive(true);
        }
    }

    #endregion //Unity Engine & Events

    public Sprite GetAvatar()
    {
        object value;
        photonPlayer.customProperties.TryGetValue("avtr", out value);
        int avatarIndex = int.Parse(value.ToString());
        return _avatarList.GetAvatarIndex(avatarIndex);
    }
	
    /// <summary>
    /// Kick this player from the current room
    /// </summary>
    public void KickFroomRoom()
    {
        _photonView.RPC("BanFromRoom", photonPlayer);
        PhotonNetwork.CloseConnection(photonPlayer);      
    }

    [PunRPC]
    private void BanFromRoom()
    {
        BannedRoomsList.bannedRooms.Add(PhotonNetwork.room.name);
    }

}
