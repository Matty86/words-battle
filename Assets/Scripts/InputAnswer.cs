﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class InputAnswer : MonoBehaviour 
{
    public delegate void Event(string category, string answer);
    public static event Event OnAnswerSent;

    #region Inspector


    #endregion //Inspector

    private Dropdown categoryDropdown;
    private InputField answerInputField;
    private Button sendButton;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        categoryDropdown = GetComponentInChildren<Dropdown>();
        answerInputField = GetComponentInChildren<InputField>();
        sendButton = GetComponentInChildren<Button>();
    }

    private void OnEnable()
    {
        Countdown.OnCountdownStart += Countdown_OnCountdownStart;
    }

    private void Countdown_OnCountdownStart()
    {
        sendButton.interactable = true;
    }

    public void OnDisable()
    {
        Countdown.OnCountdownStart -= Countdown_OnCountdownStart;
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Call a RPC to populate answer on the Master Client
    /// </summary>
    public void SendAnswer()
    {
        if(categoryDropdown.value.ToString() == string.Empty || answerInputField.text == string.Empty)
        {
            return;
        }

        if (OnAnswerSent != null) OnAnswerSent(GetSelectedCategory(), answerInputField.text);

        //disable this answer interaction
        answerInputField.interactable = false;
        sendButton.gameObject.SetActive(false);
    }   

    private string GetSelectedCategory()
    {
        return categoryDropdown.options[categoryDropdown.value].text;
    }

}
