﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

public class PopulatePlayerList : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject playerTemplate;

    [SerializeField]
    private Transform content;

    #endregion //Inspector


    #region Unity Engine & Events

    private void Start()
    {
        PopulateAll();     
    }

    private void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        SpawnPlayer(player);       
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        Remove(player);        
    }

    #endregion //Unity Engine & Events

    private void PopulateAll()
    {
        for(int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            SpawnPlayer(PhotonNetwork.playerList[i]);
        }
    }

    private void SpawnPlayer(PhotonPlayer photonPlayer)
    {
        GameObject spawnedPlayer = (GameObject)Instantiate(playerTemplate, content, false);
        Player _player = spawnedPlayer.GetComponent<Player>();
        _player.photonPlayer = photonPlayer;
    }

    private void Remove(PhotonPlayer player)
    {
        for(int i = 0; i < content.childCount; i++)
        {
            //get a reference to the player script for each child of content
            Player _player = content.GetChild(i).GetComponent<Player>();

            //if the player is found we remove it from the contents
            if(_player.photonPlayer == player)
            {
                Destroy(_player.gameObject);
                break;
            }
        }
    }

}
