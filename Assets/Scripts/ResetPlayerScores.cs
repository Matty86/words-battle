﻿/*==============================================================================

==============================================================================*/

public class ResetPlayerScores
{	
    public static void ResetAllScores()
    {
        //reset all players scores and 
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            PhotonNetwork.playerList[i].SetScore(0);
        }
    }
}
