﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class StartNextTurnButton : MonoBehaviour 
{
    public delegate void Event();
    public static event Event OnNextButtonClick;

    #region Inspector



    #endregion //Inspector

    private RoomTurns _roomTurns;
    private Image _image;
    private Button _button;
	
	#region Unity Engine & Events

    private void Awake()
    {
        _button = GetComponent<Button>();
        _roomTurns = GetComponentInParent<RoomTurns>();
        _image = GetComponent<Image>();

        Countdown.OnCountdownOver += Countdown_OnCountdownOver;
        StopGame.OnAllPlayersStopped += StopGame_OnAllPlayersStopped;
        CompleteAnswersValidation.OnAnswersValidationComplete += CompleteAnswersValidation_OnAnswersValidationComplete;
    }

    private void OnEnable()
    {
        if(_roomTurns.Turns <= 0)
        {
            _image.enabled = false;
        }       
    }

    private void StopGame_OnAllPlayersStopped()
    {
        _button.interactable = true;
    }

    private void Countdown_OnCountdownOver()
    {
        _button.interactable = true;
    }

    private void CompleteAnswersValidation_OnAnswersValidationComplete(PhotonPlayer winningPlayer)
    {
        _image.enabled = true;
    }

    private void OnDestroy()
    {
        CompleteAnswersValidation.OnAnswersValidationComplete -= CompleteAnswersValidation_OnAnswersValidationComplete;
        Countdown.OnCountdownOver -= Countdown_OnCountdownOver;
        StopGame.OnAllPlayersStopped -= StopGame_OnAllPlayersStopped;
    }

    private void Start()
    {
        if(!PhotonNetwork.isMasterClient)
        {
            gameObject.SetActive(false);
        }
    }
	
	#endregion //Unity Engine & Events
	
    /// <summary>
    /// Start the next round
    /// </summary>
    public void StartNextRound()
    {
        if (OnNextButtonClick != null) OnNextButtonClick();
    }

}
