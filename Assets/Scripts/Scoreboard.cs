﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using System.Collections.Generic;

public class Scoreboard : MonoBehaviour 
{
    #region Inspector



    #endregion //Inspector

    private Dictionary<PhotonPlayer, int> playersScores = new Dictionary<PhotonPlayer, int>();
	
	#region Unity Engine & Events
	
	private void OnEnable()
    {
        Answer.OnAnswerValidated += Answer_OnAnswerValidated;
    }

    private void Answer_OnAnswerValidated(PhotonPlayer photonPlayer, int score)
    {
        playersScores.Add(photonPlayer, score);
    }

    private void OnDisable()
    {
        Answer.OnAnswerValidated -= Answer_OnAnswerValidated;
    }

    #endregion //Unity Engine & Events



}
