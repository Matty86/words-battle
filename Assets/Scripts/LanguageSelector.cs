﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class LanguageSelector : MonoBehaviour 
{
    public SystemLanguage selectedLanguage
    {
        get
        {
            return (SystemLanguage)dropdown.value;
        }
    }

    #region Inspector



    #endregion //Inspector

    private Dropdown dropdown;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        dropdown = GetComponent<Dropdown>();
    }

    private void Start()
    {
        FillOptions();
        SetDefaultLanguge();
    }
	
	#endregion //Unity Engine & Events    
	
    private void FillOptions()
    {
        //loop throught all available language in system
        for (int i = 0; i < Enum.GetValues(typeof(SystemLanguage)).Length; i++)
        {
            //get language enum at current index
            SystemLanguage language = (SystemLanguage)i;
            //create a new drop down option
            Dropdown.OptionData newOption = new Dropdown.OptionData(language.ToString());
            //add the new option to the list option list of the dropdown
            dropdown.options.Add(newOption);
        }
    }

    private void SetDefaultLanguge()
    {
        dropdown.value = (int)SystemLanguage.English;
    }

}
