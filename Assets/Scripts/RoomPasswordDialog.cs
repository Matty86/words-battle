﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class RoomPasswordDialog : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject passwordUI;

    [SerializeField]
    private Text _wrongPassword;

    [SerializeField]
    private InputField _passwordInputField;

    #endregion //Inspector

    private string roomToJoinName;
    private string roomToJoinPassword;
	
	#region Unity Engine & Events
	
	private void OnEnable()
    {
        ClickToJoinRoom.OnAttempToJoinRoomWithPassword += ClickToJoinRoom_OnAttempToJoinRoomWithPassword;
    }

    private void ClickToJoinRoom_OnAttempToJoinRoomWithPassword(string roomName, string roomPassword)
    {
        passwordUI.SetActive(true);
        roomToJoinName = roomName;
        roomToJoinPassword = roomPassword;
    }

    private void OnDisable()
    {
        ClickToJoinRoom.OnAttempToJoinRoomWithPassword -= ClickToJoinRoom_OnAttempToJoinRoomWithPassword;
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Submit the entered password and attempt to joint if the password is correct
    /// </summary>
    public void SubmitPassword()
    {
        _wrongPassword.enabled = false;
        if(_passwordInputField.text == roomToJoinPassword)
        {
            PhotonNetwork.JoinRoom(roomToJoinName);
        }
        else
        {
            _wrongPassword.enabled = true;
        }
    }

}
