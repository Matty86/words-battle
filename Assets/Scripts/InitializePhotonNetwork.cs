﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

public class InitializePhotonNetwork : MonoBehaviour
{
    #region Inspector

    [SerializeField]
    private string gameVersion = "0.1.0";

    #endregion //Inspector



    #region Unity Engine & Events

    private void Start()
    {
        if (!PhotonNetwork.connected)
        {
            ConnectToPhoton();
        }
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Attempt to connect to Photon Network
    /// </summary>
    public void ConnectToPhoton()
    {
        bool success = PhotonNetwork.ConnectUsingSettings(gameVersion);
        if (success)
        {
            Debug.Log("<color=green>Connected to photon!</color>");
        }
        else
        {
            Debug.Log("<color=orange>Couldn't connect to photon, try again!</color>");
        }
    }

}
