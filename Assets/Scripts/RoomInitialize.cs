﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

/// <summary>
/// Initialize room
/// </summary>
public class RoomInitialize : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject playerUI;

    [SerializeField]
    private GameObject hostUI;
	
	#endregion //Inspector


	
	#region Unity Engine & Events
	
	private void Start()
    {
        if(PhotonNetwork.isMasterClient)
        {
            hostUI.SetActive(true);
            playerUI.SetActive(false);
        }
        else
        {
            playerUI.SetActive(true);
            hostUI.SetActive(false);
        }
    }
	
	#endregion //Unity Engine & Events
	
}
