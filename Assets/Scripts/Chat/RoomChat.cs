﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonView))]
public class RoomChat : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject chatMessageTemplate;

    #endregion //Inspector

    private PhotonView _photonView;
    private InputField _messageInputField;
    private Transform content;

    #region Unity Engine & Events

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _messageInputField = GetComponentInChildren<InputField>();
        content = GetComponentInChildren<ScrollRect>().content;
    }

    #endregion //Unity Engine & Events

    /// <summary>
    /// Send the message written in the input field
    /// </summary>
    public void SendMessage()
    {
        if (_messageInputField.text != string.Empty)
        {
            _photonView.RPC("SendMessage_RPC", PhotonTargets.All, PhotonNetwork.player, _messageInputField.text);
        }
    }

    [PunRPC]
    private void SendMessage_RPC(PhotonPlayer player, string message)
    {
        GameObject sentMessage = (GameObject)Instantiate(chatMessageTemplate, content, false);
        ChatMessageBuilder _chatMessageBuilder = sentMessage.GetComponent<ChatMessageBuilder>();
        _chatMessageBuilder.WriteMessage(player, message);
        _messageInputField.text = string.Empty;
    }

}
