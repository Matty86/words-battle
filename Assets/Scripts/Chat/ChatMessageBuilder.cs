﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class ChatMessageBuilder : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private Text _messageText;

    [SerializeField]
    private Text _playerNameText;

    #endregion //Inspector

	
	#region Unity Engine & Events
	
	
	#endregion //Unity Engine & Events
	
    /// <summary>
    /// Write the message in this message template
    /// </summary>
    /// <param name="message">The message to write</param>
    public void WriteMessage(PhotonPlayer player , string message)
    {
        _playerNameText.text = player.name;
        _messageText.text = message;
    }

}
