﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class PopulateAnswers : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject answerTemplate;

    [SerializeField]
    private Transform[] content;

    #endregion //Inspector

    private PhotonView _photonView;
	
	#region Unity Engine & Events

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
    }

	private void OnEnable()
    {
        InputAnswer.OnAnswerSent += InputAnswer_OnAnswerSent;
    }

    private void InputAnswer_OnAnswerSent(string category, string answer)
    {
        _photonView.RPC("ShowAnswer", PhotonTargets.AllBuffered, category, answer, PhotonNetwork.player);
    }

    private void OnDisable()
    {
        InputAnswer.OnAnswerSent -= InputAnswer_OnAnswerSent;
    }

    #endregion //Unity Engine & Events

    [PunRPC]
    private void ShowAnswer(string category, string answer, PhotonPlayer player)
    {
        for (int i = 0; i < content.Length; i++)
        {
            GameObject spawnedAnswer = (GameObject)Instantiate(answerTemplate, content[i], false);
            Answer _answer = spawnedAnswer.GetComponent<Answer>();
            _answer.photonPlayer = player;
            _answer.SetAnswer(answer);
            _answer.SetCategory(category);
            _answer.replyTime = Countdown.Instance.timeLeft;
            _answer.SetCategory(category);

            if (Debug.isDebugBuild)
            {
                Debug.Log("Answer sent from " + player.name);
            }
        }
    }

}
