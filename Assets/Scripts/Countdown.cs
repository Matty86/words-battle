﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class Countdown : Singleton<Countdown> 
{
    public delegate void Event();
    public static event Event OnCountdownStart;
    public static event Event OnCountdownOver;

    #region Inspector

    [SerializeField]
    private Button _stopButton;

    [SerializeField]
    private Button _startNextButton;

    [SerializeField]
    private Button _validateButton;

    #endregion //Inspector

    [HideInInspector]
    public int timeLeft;

    private WaitForSeconds wait_1_seconds = new WaitForSeconds(1);

    private Text _text;
    private PhotonView _photonView;
    private RoomTurns _roomTurns;
	
	#region Unity Engine & Events
	
	public override void Awake()
    {
        base.Awake();
        _text = GetComponent<Text>();
        _photonView = GetComponent<PhotonView>();
        _roomTurns = GetComponentInParent<RoomTurns>();
    }

    private void OnEnable()
    {
        StartGameButton.OnStartButtonClick += StartGameButton_OnStartButtonClick;
        ShowScoreboard.OnScoreboardHide += ShowScoreboard_OnScoreboardHide;
        StopGame.OnAllPlayersStopped += StopGame_OnAllPlayersStopped;
    }

    private void StopGame_OnAllPlayersStopped()
    {
        _photonView.RPC("StopTimer", PhotonTargets.All);
    }

    private void StartNextTurnButton_OnNextButtonClick()
    {
        ResetCountDown();
    }

    private void ShowScoreboard_OnScoreboardHide()
    {
        if (PhotonNetwork.isMasterClient)
        {
            //Send a RPC to start the timer
            _photonView.RPC("StartTimer", PhotonTargets.All);
        }
    }

    private void StartGameButton_OnStartButtonClick()
    {
        if (PhotonNetwork.isMasterClient)
        {
            //Send a RPC to start the timer
            _photonView.RPC("StartTimer", PhotonTargets.All);
        }
    }

    private void OnDisable()
    {
        StartGameButton.OnStartButtonClick -= StartGameButton_OnStartButtonClick;
        ShowScoreboard.OnScoreboardHide -= ShowScoreboard_OnScoreboardHide;
        StopGame.OnAllPlayersStopped -= StopGame_OnAllPlayersStopped;
    }

    private void Start()
    {
        InitializeRoomTimer();
    }

    #endregion //Unity Engine & Events

    [PunRPC]
    private void StartTimer()
    {
        StartCoroutine(StartCountdown());
        _stopButton.interactable = true;
    }

    [PunRPC]
    private void StopTimer()
    {
        //Take timer to 0
        timeLeft = 0;
    }

    private void InitializeRoomTimer()
    {
        if (PhotonNetwork.inRoom)
        {
            ResetCountDown();
        }
        else
        {
            Debug.Log("<color=red>The countdown won't start because you are not in a room!</color>");
        }
    }    

    private int GetRoomTurnDuration()
    {
        //Get the current amount of time from photon room custom properties
        object value;
        PhotonNetwork.room.customProperties.TryGetValue("time", out value);
        return int.Parse(value.ToString());
    }

    private void ResetCountDown()
    {
        timeLeft = GetRoomTurnDuration();
        _text.text = timeLeft.ToString();
    }

    private IEnumerator StartCountdown()
    {
        if (OnCountdownStart != null) OnCountdownStart();

        while(timeLeft > 0)
        {
            yield return wait_1_seconds;
            timeLeft--;
            _text.text = timeLeft.ToString();
        }
        _startNextButton.interactable = true;

        if (_roomTurns.Turns <= 0 && PhotonNetwork.isMasterClient)
        {
            _validateButton.interactable = true;
        }

        if (OnCountdownOver != null) OnCountdownOver();
        ResetCountDown();
    }

}
