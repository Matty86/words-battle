﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListRoomCategories : MonoBehaviour 
{
    #region Inspector



    #endregion //Inspector

    private Dropdown _dropdown;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _dropdown = GetComponent<Dropdown>();
    }

    private void Start()
    {
        FillDropdownOptions();
    }
	
	#endregion //Unity Engine & Events
	
    private void FillDropdownOptions()
    {
        //remove all options before adding new ones
        _dropdown.options.Clear();
        //create a new list of options for the dropdown
        List<string> options = new List<string>();
        // Get catN (amount of categories) from current room custom properties
        int categoriesAmount = int.Parse(GetRoomCustomPropertyValue("catN"));
        for (int i = 0; i < categoriesAmount; i++)
        {
            //get current category ad index from room custom properties and ad it to the list of options
            string optionValue = GetRoomCustomPropertyValue("cat" + i);
            options.Insert(i, optionValue);
        }
        //add the options to the dropdown
        _dropdown.AddOptions(options);
    }

    private string GetRoomCustomPropertyValue(string propertyKey)
    {
        object value;
        bool success = PhotonNetwork.room.customProperties.TryGetValue(propertyKey, out value);
        if (success)
        {
            return value.ToString();
        }
        else
        {
            Debug.Log(propertyKey + " was not found on the room custom properties!");
            return string.Empty;
        }
    }
}
