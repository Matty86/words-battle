﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class PopulateInputAnswers : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject inputAnswerTemplate;

    #endregion //Inspector

    private ScrollRect _scrollView;

    #region Unity Engine & Events

    private void Awake()
    {
        _scrollView = GetComponent<ScrollRect>();
    }

    private void OnEnable()
    {
        InputAnswer.OnAnswerSent += InputAnswer_OnAnswerSent;
    }

    private void InputAnswer_OnAnswerSent(string category, string answer)
    {
        AddAnswerField();
    }

    private void OnDisable()
    {
        InputAnswer.OnAnswerSent -= InputAnswer_OnAnswerSent;
    }

    #endregion //Unity Engine & Events

    private void AddAnswerField()
    {
        Instantiate(inputAnswerTemplate, _scrollView.content, false);
    }

}
