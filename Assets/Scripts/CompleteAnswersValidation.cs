﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CompleteAnswersValidation : MonoBehaviour 
{
    public delegate void AnswerValidationEvent(PhotonPlayer winningPlayer);
    public static event AnswerValidationEvent OnAnswersValidationComplete;

    #region Inspector



    #endregion //Inspector

    private RoomTurns _roomTurns;
    private Image _image;
    private Button _button;

    #region Unity Engine & Events

    private void Awake()
    {
        _roomTurns = GetComponentInParent<RoomTurns>();
        _image = GetComponent<Image>();
        _button = GetComponent<Button>();

        StopGame.OnAllPlayersStopped += StopGame_OnAllPlayersStopped;
        Countdown.OnCountdownOver += Countdown_OnCountdownOver;
    }

    private void OnEnable()
    {
        //Enable the graphics only if it is the last turn
        if(_roomTurns.Turns <= 0)
        {
            _image.enabled = true;
        }
        else
        {
            _image.enabled = false;
        }      
    }

    private void Countdown_OnCountdownOver()
    {
        CheckInteractable();
    }

    private void StopGame_OnAllPlayersStopped()
    {
        //CheckInteractable();
    }

    private void OnDestroy()
    {
        StopGame.OnAllPlayersStopped -= StopGame_OnAllPlayersStopped;
        Countdown.OnCountdownOver -= Countdown_OnCountdownOver;
    }

    private void Start()
    {
        if(!PhotonNetwork.isMasterClient)
        {
            Destroy(gameObject);
        }
    }

    #endregion //Unity Engine & Events

    private void CheckInteractable()
    {
        if (_roomTurns.Turns <= 0)
        {
            _button.interactable = true;
        }
    }

    public void CompleteValidation()
    {
        if (OnAnswersValidationComplete != null) OnAnswersValidationComplete(FindWinner());
        _image.enabled = false;
    }

    private PhotonPlayer FindWinner()
    {
        List<int> playerScores = new List<int>();

        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            playerScores.Add(PhotonNetwork.playerList[i].GetScore());
        }

        //sort the list descending order
        playerScores.Sort();
        playerScores.Reverse();

        //search for all players in room
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            //if the score of the player is equal to the first score on the list
            if (PhotonNetwork.playerList[i].GetScore() == playerScores[0])
            {
                //we found the winning player, we call an event and pass a reference to the winnin player
                return PhotonNetwork.playerList[i];
            }
        }

        //nothing was found
        return null;
    }

}
