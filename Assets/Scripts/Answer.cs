﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class Answer : MonoBehaviour 
{
    public delegate void AnswerValidateEvent(PhotonPlayer photonPlayer, int score);
    public static event AnswerValidateEvent OnAnswerValidated;

    #region Inspector

    [SerializeField]
    private Text categoryField;

    [SerializeField]
    private Text answerField;

    public GameObject validateToggle;

    #endregion //Inspector

    /// <summary>
    /// The time it took to enter the answer
    /// </summary>
    [HideInInspector]
    public int replyTime;

    /// <summary>
    /// The player that answered
    /// </summary>
    public PhotonPlayer photonPlayer;
	
	#region Unity Engine & Events
	
    private void Start()
    {
        //Remove the validate toggle if this is not the master client
        if(!PhotonNetwork.isMasterClient)
        {
            Destroy(validateToggle);
        }
    }
	
	#endregion //Unity Engine & Events
	
    /// <summary>
    /// Set the answer
    /// </summary>
    /// <param name="answer">The answer</param>
    public void SetAnswer(string answer)
    {
        answerField.text = answer;
    }

    /// <summary>
    /// Set the category
    /// </summary>
    /// <param name="category">The category of the answer</param>
    public void SetCategory(string category)
    {
        categoryField.text = category;
    }

    /// <summary>
    /// Validate current answer and add a score to the player
    /// </summary>
    public void ValidateAnswer()
    {
        int score = 10 * replyTime;
        photonPlayer.AddScore(score);

        if (OnAnswerValidated != null) OnAnswerValidated(photonPlayer, score);

        if(Debug.isDebugBuild)
        {
            Debug.Log("Validated answer from " + photonPlayer.name + " his score is now: " + photonPlayer.GetScore());
        }
    }

}
