﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class ShowWinner : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private GameObject winningPlayerUI;

    #endregion //Inspector

    private PhotonView _photonView;
    private WinnerPlayer _winnerPlayer;

    #region Unity Engine & Events

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        _winnerPlayer = GetComponentInChildren<WinnerPlayer>(true);
    }

	private void OnEnable()
    {
        CompleteAnswersValidation.OnAnswersValidationComplete += CompleteAnswersValidation_OnAnswersValidationComplete;
    }

    private void CompleteAnswersValidation_OnAnswersValidationComplete(PhotonPlayer winningPlayer)
    {
        _photonView.RPC("ShowWinningPlayer", PhotonTargets.All, winningPlayer);
    }

    private void OnDisable()
    {
        CompleteAnswersValidation.OnAnswersValidationComplete -= CompleteAnswersValidation_OnAnswersValidationComplete;
    }

    #endregion //Unity Engine & Events

    [PunRPC]
    private void ShowWinningPlayer(PhotonPlayer winningPlayer)
    {
        winningPlayerUI.SetActive(true);
        _winnerPlayer.SetWinningPlayer(winningPlayer);
    }

}
