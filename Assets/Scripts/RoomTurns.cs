﻿/*==============================================================================

==============================================================================*/

using UnityEngine;

public class RoomTurns : MonoBehaviour
{
    public int Turns { get; private set; }
	
	#region Unity Engine & Events
	
    private void OnEnable()
    {
        Countdown.OnCountdownStart += Countdown_OnCountdownStart;
    }

    private void Countdown_OnCountdownStart()
    {
        Turns--;
    }

    private void OnDisable()
    {
        Countdown.OnCountdownStart -= Countdown_OnCountdownStart;
    }

    private void Start()
    {
        Turns = GetRoomTurns();
    }

    #endregion //Unity Engine & Events

    private int GetRoomTurns()
    {
        object value;
        PhotonNetwork.room.customProperties.TryGetValue("turns", out value);
        return int.Parse(value.ToString());
    }

}
