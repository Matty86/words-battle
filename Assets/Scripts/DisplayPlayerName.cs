﻿/*==============================================================================

==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

public class DisplayPlayerName : MonoBehaviour 
{
    #region Inspector

    [SerializeField]
    private Player _player;

    #endregion //Inspector

    private Text _text;
	
	#region Unity Engine & Events
	
	private void Awake()
    {
        _text = GetComponent<Text>();
    }
	
    private void Start()
    {
        _text.text = _player.photonPlayer.name;
    }

	#endregion //Unity Engine & Events
	
}
